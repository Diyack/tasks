public class Student extends Human {
	private int yearOfStudy;

	public void setYearOfStudy(int year){
		this.yearOfStudy = year;
	}
	public void upYearOfStudy(){
		this.yearOfStudy += 1;
	}
	public int getYearOfStudy(){
		return yearOfStudy;
	}
}

class Human {
	private String name;
	private int age;
	private Boolean gender;
	private double weight;

	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	public void setAge(int age){
		this.age = age;
	}
	public int getAge(){
		return age;
	}
	public void setGender(Boolean gender){
		this.gender = gender;
	}
	public Boolean getGender(){
		return gender;
	}
	public void setWeight(double weight){
		this.weight = weight;
	}
	public double getWeight(){
		return weight;
	}
}
