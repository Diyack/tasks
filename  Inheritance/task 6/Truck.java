public class Truck extends Automobile {
	private double carrying;
	private String mark;

	public void setCarrying(double carrying){
		this.carrying = carrying;
	}
	public double getCarrying(){
		return carrying;
	}
	public void setMark(String mark){
		this.mark = mark;
	}
	public String getMark(){
		return mark;
	}
}

class Automobile {
	private String mark;
	private double weight;
	private double power;

	public void setPower(double power){
		this.power = power;
	}
	public double getPower(){
		return power;
	}
}
