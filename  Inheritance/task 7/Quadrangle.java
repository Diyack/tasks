public abstract class Quadrangle {
	private double firstCorner;
	private double secondCorner;
	private double thirdCorner;
	private double fourthCorner;
	private double firstSide;
	private double secondSide;
	private double thirdSide;
	private double fourthSide;

	public void setFirstCorner(double firstCorner){
		this.firstCorner = firstCorner;
	}
	public double getFirstCorner(){
		return firstCorner;
	}
	public void setSecondCorner(double secondCorner){
		this.secondCorner = secondCorner;
	}
	public double getSecondCorner(){
		return secondCorner;
	}
	public void setThirdCorner(double thirdCorner){
		this.thirdCorner = thirdCorner;
	}
	public double getThirdCorner(){
		return thirdCorner;
	}
	public void setFourthCorner(double fourthCorner){
		this.fourthCorner = fourthCorner;
	}
	public double getFourthCorner(){
		return fourthCorner;
	}
	public void setFirstSide(double firstCorner){
		this.firstSide = firstSide;
	}
	public double getFirstSide(){
		return firstSide;
	}
	public void setSecondSide(double secondCorner){
		this.secondSide = secondSide;
	}
	public double getSecondSide(){
		return secondSide;
	}
	public void setThirdSide(double thirdCorner){
		this.thirdSide = thirdSide;
	}
	public double getThirdSide(){
		return thirdSide;
	}
	public void setFourthSide(double fourthCorner){
		this.fourthSide = fourthSide;
	}
	public double getFourthSide(){
		return fourthSide;
	}
}

abstract class ConvexQuadrilateral extends Quadrangle {
	public final double sumOfCorner = 360;
}

class Trapezoid extends ConvexQuadrilateral {

	Trapezoid(double firstSide, double secondSide, double thirdSide, double fourthSide, double firstCorner, double secondCorner, double thirdCorner, double fourthCorner){
		setFirstSide(firstSide);
		setSecondSide(secondSide);
		setThirdSide(thirdSide);
		setFourthSide(fourthSide);
		setFirstCorner(firstCorner);
		setSecondCorner(secondCorner);
		setThirdCorner(thirdCorner);
		setFourthCorner(fourthCorner);
	}

}

class Parallelogram extends ConvexQuadrilateral {

	Parallelogram(double firstSide, double secondSide, double firstCorner, double secondCorner){
		setFirstSide(firstSide);
		setSecondSide(secondSide);
		setThirdSide(firstSide);
		setFourthSide(secondSide);
		setFirstCorner(firstCorner);
		setSecondCorner(secondCorner);
		setThirdCorner(firstCorner);
		setFourthCorner(secondCorner);	
	}

}

class Rhombus extends ConvexQuadrilateral {

	Rhombus(double firstSide, double firstCorner, double secondCorner){
		setFirstSide(firstSide);
		setSecondSide(firstSide);
		setThirdSide(firstSide);
		setFourthSide(firstSide);
		setFirstCorner(firstCorner);
		setSecondCorner(secondCorner);
		setThirdCorner(firstCorner);
		setFourthCorner(secondCorner);
	}

}

class Rectangle extends ConvexQuadrilateral {

	Rectangle(double firstSide, double secondSide){
		setFirstSide(firstSide);
		setSecondSide(secondSide);
		setThirdSide(firstSide);
		setFourthSide(secondSide);
		setFirstCorner(90);
		setSecondCorner(90);
		setThirdCorner(90);
		setFourthCorner(90);
	}
}

class Foursquare extends ConvexQuadrilateral {

	Foursquare(double firstSide){
		setFirstSide(firstSide);
		setSecondSide(firstSide);
		setThirdSide(firstSide);
		setFourthSide(firstSide);
		setFirstCorner(90);
		setSecondCorner(90);
		setThirdCorner(90);
		setFourthCorner(90);
	}
}
