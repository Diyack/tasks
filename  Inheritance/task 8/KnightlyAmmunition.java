import java.util.*;

public class KnightlyAmmunition {
	private Helmet helmet;
	private Cuirass cuirass;
	private Legguards legguards;
	private Boots boots;
	private Mittens mittens;
	private Shield shield;
	private Weapon weapon;

	private int attack;
	private int armor;

	public void setAttack(){
		this.attack = (this.weapon.getMinDamage() + this.weapon.getMaxDamage())/2;
	}
	public int getAttack(){
		return attack;
	}
	public void setArmor(){
		this.armor = this.helmet.getArmorRate() + this.cuirass.getArmorRate() + this.legguards.getArmorRate() + this.boots.getArmorRate() + this.mittens.getArmorRate() + this.shield.getArmorRate();
	}
	public int getArmor(){
		return armor;
	}

	public Helmet getHelmet(){
		return helmet;
	}
	public void setHelmet(Helmet helmet){
		this.helmet = helmet;
	}
	public Cuirass getCuirass(){
		return cuirass;
	}
	public void setCuirass(Cuirass cuirass){
		this.cuirass = cuirass;
	}
	public Legguards getLegguards(){
		return legguards;
	}
	public void setLegguards(Legguards legguards){
		this.legguards = legguards;
	}
	public Boots getBoots(){
		return boots;
	}
	public void setBoots(Boots boots){
		this.boots = boots;
	}
	public Mittens getMittens(){
		return mittens;
	}
	public void setMittens(Mittens mittens){
		this.mittens = mittens;
	}
	public Shield getShield(){
		return shield;
	}
	public void setShield(Shield shield){
		this.shield = shield;
	}
	public Weapon getWeapon(){
		return weapon;
	}
	public void setWeapon(Weapon weapon){
		this.weapon = weapon;
	}

	public static void costCalculation(KnightlyAmmunition ob){
		long sumPrise = 0;
		sumPrise = ob.getHelmet().getPrise() + ob.getCuirass().getPrise() + ob.getLegguards().getPrise() + ob.getBoots().getPrise() + ob.getMittens().getPrise() + ob.getShield().getPrise() + ob.getWeapon().getPrise();
		System.out.println("Суммарная стоимость экипировки: " + sumPrise);
	}
	public static void sortByWeight(KnightlyAmmunition ob){
		AmmunitionList[] ammArr = new AmmunitionList[7];
		ammArr[0] = new AmmunitionList(ob.getWeapon());
		ammArr[1] = new AmmunitionList(ob.getShield());
		ammArr[2] = new AmmunitionList(ob.getBoots());
		ammArr[3] = new AmmunitionList(ob.getHelmet());
		ammArr[4] = new AmmunitionList(ob.getCuirass());
		ammArr[5] = new AmmunitionList(ob.getLegguards());
		ammArr[6] = new AmmunitionList(ob.getMittens());

		Arrays.sort(ammArr);
		System.out.println(Arrays.toString(ammArr));
	}

	public static void searchByPrise(long startingPrise, long finalPrise, KnightlyAmmunition ob){
		Base base1 = new Base();
		base1.addAmmunitionList(new AmmunitionList(ob.getWeapon()));
		base1.addAmmunitionList(new AmmunitionList(ob.getShield()));
		base1.addAmmunitionList(new AmmunitionList(ob.getBoots()));
		base1.addAmmunitionList(new AmmunitionList(ob.getHelmet()));
		base1.addAmmunitionList(new AmmunitionList(ob.getCuirass()));
		base1.addAmmunitionList(new AmmunitionList(ob.getLegguards()));
		base1.addAmmunitionList(new AmmunitionList(ob.getMittens()));

		ArrayList<AmmunitionList> myBase = base1.getBase();

		for (AmmunitionList dataBase : myBase){
			System.out.println(dataBase.searchPrise(startingPrise, finalPrise));
		}
	}

	public static void main(String[] args) {
		Helmet helmet1 = new Helmet();
		Cuirass cuirass1 = new Cuirass();
		Legguards legguards1 = new Legguards();
		Boots boots1 = new Boots();
		Mittens mittens1 = new Mittens();
		Shield shield1 = new Shield();
		Weapon weapon1 = new Weapon();

		KnightlyAmmunition knightlyAmmunition1 = new KnightlyAmmunition();
		knightlyAmmunition1.setHelmet(helmet1);
		knightlyAmmunition1.setCuirass(cuirass1);
		knightlyAmmunition1.setLegguards(legguards1);
		knightlyAmmunition1.setBoots(boots1);
		knightlyAmmunition1.setMittens(mittens1);
		knightlyAmmunition1.setShield(shield1);
		knightlyAmmunition1.setWeapon(weapon1);

		helmet1.setPrise(100);
		cuirass1.setPrise(100);
		legguards1.setPrise(75);
		boots1.setPrise(50);
		mittens1.setPrise(50);
		shield1.setPrise(300);
		weapon1.setPrise(500);

		helmet1.setName("Шлем");
		cuirass1.setName("Кираса");
		legguards1.setName("Набедренники");
		boots1.setName("Сапоги");
		mittens1.setName("Перчатки");
		shield1.setName("Щит");
		weapon1.setName("Оружие");

		helmet1.setWeight(3.0);
		cuirass1.setWeight(10.0);
		legguards1.setWeight(5.0);
		boots1.setWeight(3.5);
		mittens1.setWeight(1.5);
		shield1.setWeight(10.0);
		weapon1.setWeight(2.0);

		costCalculation(knightlyAmmunition1);
		sortByWeight(knightlyAmmunition1);
		System.out.println("Предметы в заданном ценовом диапазоне:");
		searchByPrise(50, 150, knightlyAmmunition1);
	}
}

class Ammunition {
	private String name;
	private String material;
	private double weight;
	private Size size;
	private int durability;
	private long prise;

	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	public int getDurability(){
		return durability;
	}
	public void setDurability(int durability){
		this.durability = durability;
	}
	public String getMaterial(){
		return material;
	}
	public void setMaterial(String material){
		this.material = material;
	}
	public double getWeight(){
		return weight;
	}
	public void setWeight(double weight){
		this.weight = weight;
	}
	public long getPrise(){
		return prise;
	}
	public void setPrise(long prise){
		this.prise = prise;
	}
}

class Armor extends Ammunition {
	private int armorRate;

	public int getArmorRate(){
		return armorRate;
	}
	public void setArmorRate(int armorRate){
		this.armorRate = armorRate;
	}
}

class Weapon extends Ammunition {
	private int minDamage; 
	private int maxDamage;
	private double critRate;
	private double critChance;
	private String type;

	public String getType(){
		return type;
	}
	public void setType(String type){
		this.type = type;
	}
	public int getMinDamage(){
		return minDamage;
	}
	public void setMinDamage(int minDamage){
		this.minDamage = minDamage;
	}
	public int getMaxDamage(){
		return maxDamage;
	}
	public void setMaxDamage(int maxDamage){
		this.maxDamage = maxDamage;
	}
	public double getCritRate(){
		return critRate;
	}
	public void setCritRate(double critRate){
		this.critRate = critRate;
	}
	public double getCritChance(){
		return critChance;
	}
	public void setCritChance(double critChance){
		this.critChance = critChance;
	}

	Weapon(){

	}
}

class Shield extends Armor {
	Shield(){

	}
}

class Helmet extends Armor {
	Helmet(){

	}
}

class Cuirass extends Armor {
	Cuirass(){

	}
}

class Legguards extends Armor {
	Legguards(){

	}
}

class Boots extends Armor {
	Boots(){

	}
}

class Mittens extends Armor {
	Mittens(){

	}
}

class Size {
	private double width;
	private double length;
	private double height;

	public void setWidth(double width){
		this.width = width;
	}
	public double getWidth(){
		return width;
	}
	public void setLength(double length){
		this.length = length;
	}
	public double getLength(){
		return length;
	}
	public void setHeight(double height){
		this.height = height;
	}
	public double getHeight(){
		return height;
	}
}

class AmmunitionList implements Comparable<AmmunitionList>{
	private String name;
	private double weight;
	private long prise;

	public String getName(){
		return name;
	}
	public double getWeight(){
		return weight;
	}
	public long getPrise(){
		return prise;
	}

	AmmunitionList(Weapon ob){
		this.name = ob.getName();
		this.weight = ob.getWeight();
		this.prise = ob.getPrise();
	}

	AmmunitionList(Shield ob){
		this.name = ob.getName();
		this.weight = ob.getWeight();
		this.prise = ob.getPrise();
	}

	AmmunitionList(Helmet ob){
		this.name = ob.getName();
		this.weight = ob.getWeight();
		this.prise = ob.getPrise();
	}

	AmmunitionList(Cuirass ob){
		this.name = ob.getName();
		this.weight = ob.getWeight();
		this.prise = ob.getPrise();
	}

	AmmunitionList(Legguards ob){
		this.name = ob.getName();
		this.weight = ob.getWeight();
		this.prise = ob.getPrise();
	}

	AmmunitionList(Boots ob){
		this.name = ob.getName();
		this.weight = ob.getWeight();
		this.prise = ob.getPrise();
	}

	AmmunitionList(Mittens ob){
		this.name = ob.getName();
		this.weight = ob.getWeight();
		this.prise = ob.getPrise();
	}

	public String searchPrise(long a, long b){
		if(this.prise >= a && this.prise <= b)
			return this.name + " Вес: " + this.weight + " Цена: " + this.prise;
		else return "";
	}

	@Override
	public int compareTo(AmmunitionList ob){
		return (int)(this.weight - ob.weight);
	}
	@Override
	public String toString(){
		return this.name + " Вес: " + this.weight + " Цена: " + this.prise;
	}
}

class Base{
	private ArrayList<AmmunitionList> base;

	Base(){
		base = new ArrayList<>();
	}

	public void addAmmunitionList(AmmunitionList ammunitionlist){
		base.add(ammunitionlist);
	}
	public ArrayList<AmmunitionList> getBase(){
		return base;
	}
}