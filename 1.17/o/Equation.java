import java.util.Scanner;

//sqrt(1 - pow(sin(x)), 2))

public class Equation {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        System.out.println("Введите число: ");
        double input = in.nextDouble();
        double Sin = Math.sin(input);
        double Pow = Math.pow(Sin, 2);
        double result = Math.sqrt(1 - Pow);
        //System.out.println("Синус " +Sin);
        //System.out.println("Квадрат синуса " +Pow);
        System.out.println("Результат " +result);
    }
}
