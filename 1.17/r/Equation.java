import java.util.Scanner;

// (sqrt(x+1)+sqrt(x-1))/2*sqrt(x)

public class Equation {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        System.out.println("Введите число: ");
        double x = in.nextDouble();
        double Sqrt1 = Math.sqrt(x);
        double Sqrt2 = Math.sqrt(x + 1);
        double Sqrt3 = Math.sqrt(x - 1);
        double result = (Sqrt2 + Sqrt3)/(2*Sqrt1);        
        //System.out.println("корень х = " +Sqrt1);
        //System.out.println("корень х + 1 = " +Sqrt2);
        //System.out.println("корень х - 1 = " +Sqrt3);
        System.out.println("Результат " +result);
    }
}