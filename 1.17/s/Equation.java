import java.util.Scanner;

// |x|+|x+1|

public class Equation {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        System.out.println("Введите число: ");
        double x = in.nextDouble();
        double result = Math.abs(x) + Math.abs(x+1);
        System.out.println("Результат " +result);
    }
}