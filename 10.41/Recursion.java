import java.util.*;

class Factorial {
 
    // это рекурсивный метод
    int fact(int n) {
 	int result;
 
  	if(n == 1) return 1;
        result = fact(n - 1) * n;
  
        return result;
 
    }
}
 
class Recursion {
 
    public static void main(String args[]) {
       
        Factorial fact = new Factorial();

        Scanner in = new Scanner(System.in);
        System.out.println("Введите число ");
        int n = in.nextInt();
 
        System.out.println("Факториал " + n + " равен " + fact.fact(n));

    }
 
}
