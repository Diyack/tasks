import java.util.*;

public class InPow {
	public static int pow(int value, int powValue) {
   		if (powValue == 1) {
    	return value;
   		} else {
       	return value * pow(value, powValue - 1);
   		}
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Введите число: ");
		int x = in.nextInt();
		System.out.println("Введите степень числа: ");
		int n = in.nextInt();
		System.out.println(x + " в степени " + n + " = " + pow(x,n));
	}
}
