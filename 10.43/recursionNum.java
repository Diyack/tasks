import java.util.*;

public class recursionNum {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
    	System.out.println("Введите число: ");
    	int n = in.nextInt();
        System.out.println("Сумма чисел числа: " + sum(n));
        System.out.println("Количество чисел числа: " + num(n));
    }
    public static int sum(int n){
    	if (n < 10) return n;
    	return n % 10 + sum(n / 10);

    }
    public static int num(int n){
    	if(n < 10) return 1;
    	return 1 + num(n/10);
    }
}
