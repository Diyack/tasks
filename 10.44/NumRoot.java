import java.util.*;

public class NumRoot {
	public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
        	System.out.println("Введите число больше или равное 0: ");
            int n=sc.nextInt();
            if (n>= 0 && n <= 2147483647)
                System.out.println(root(n));
            else
                System.out.println("error");
        }
    }
    
    private static int root(int in) {
        int sum = 0;
        int tmp = in;
        while (tmp > 0) {
            sum += tmp%10;
            tmp = tmp/10;
        }
        if (sum >= 10)
            sum = root(sum);
        return sum;
    }
}
