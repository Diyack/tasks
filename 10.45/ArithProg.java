import java.util.*;

public class ArithProg {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        System.out.println("Введите число: ");
        int x = in.nextInt();
        System.out.println("Введите разность арифметической прогресии: ");
        int r = in.nextInt();
        System.out.println("Введите количество чисел в прогресии: ");
        int n = in.nextInt();
        System.out.println(n + " элемент прогресии равен: " + num(x,r,n));
        System.out.println("Сумма " + n + " элементов прогрессии равна: " + sum(x,r,n));
    }
    public static int num(int x, int r, int n) {
    	if (n == 1) return x;
    	else return x + (n-1) * r;

    }
    public static int sum(int x, int r, int n) {
    	int sum = 0;
    	if (n == 1) return x;
    	else return sum = ((((2 * x) + r * (n-1))) / 2 * n);

    }
}
