import java.util.*;

public class fibonacci {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
    	System.out.println("Введите порядковый номер фибоначчи: ");
    	int n = in.nextInt();
        System.out.println(fib(n));
    }
    public static int fib(int n){
    	int nth = 0;
    	int nMinus2 = 0; // n-2
    	int nMinus1 = 1; // n-1
    	if (n == 0) return 0;
    	if (n == 1) return 1;
    	for (int i = 2; i <= n; ++i) {
    		nth = nMinus1 + nMinus2;
    		nMinus2 = nMinus1;
    		nMinus1 = nth;
    	}
    	return nth;
    }
}
