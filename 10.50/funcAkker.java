import java.util.*;

public class funcAkker {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
    	System.out.println("Введите число n: ");
    	int n = in.nextInt();
    	System.out.println("Введите число m: ");
    	int m = in.nextInt();
        System.out.println(Akker(n, m));
    }
    public static int Akker(int n, int m){
    	if (n == 0) return m + 1;
    	else if ((n != 0) && (m == 0)) return Akker(n-1, 1);
    	else return Akker(n - 1, Akker(n, m - 1));
    }
}
