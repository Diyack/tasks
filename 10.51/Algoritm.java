public class Algoritm {
    public static void main(String[] args) {
    	int n = 5;
        System.out.println("Вывод А: ");
        getA(n);
        System.out.println("Вывод Б: ");
        getB(n);
        System.out.println("Вывод В: ");
        getC(n);
    }
    public static void getA(int n){
    	if (n > 0){
            System.out.println(n);
            getA(n-1);
        }
        else System.out.println();
    }
    public static void getB(int n){
    	if (n > 0){
            getB(n-1);
            System.out.println(n);
        }
        else System.out.println();
    }
    public static void getC(int n){
        if (n > 0){
        System.out.println(n);
        getC(n-1);
        System.out.println(n);
        }
        else System.out.println();
    }
}
