import java.util.*;

public class overageNatur {
    public static int consoleInputFirstNumber() {
        Scanner scan = new Scanner(System. in );
        System.out.print("Введите число: ");
        if (scan.hasNextInt()) {
            return scan.nextInt();
        } else {
            System.out.println("Введено не число.");
            return consoleInputFirstNumber();
        }
    }

    public static void numberReverseOrder(int number) {
        if (number == 0) {
            return;
        } else {
            System.out.println(number % 10);
            numberReverseOrder(number / 10);
        }
    }

    public static void consoleOutput(int result) {
        System.out.println(result);
    }

    public static void main(String[] args) {
        int input = consoleInputFirstNumber();
        numberReverseOrder(input);
    }
}