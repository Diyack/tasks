import java.util.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ArrIn {
    public static void main(String[] args) throws IOException {
        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
        String x = br.readLine();
        
        String[] str = x.split(" ");
        
        int arrayInt[] = new int[0];       
     
        for (int i = 0; i < str.length; i++) {          
            if(str[i].matches("^-?\\d+$")) {
                int n = Integer.parseInt(str[i]);
                if (n != 0){
                    int temp[] = arrayInt;                  
                    arrayInt = new int[temp.length+1];          
                    for(int j = 0; j < temp.length; j++) {
                        arrayInt[j] = temp[j];
                    }           
                    arrayInt[temp.length] = n;
                }
                else break;
            }
        }
        for (int i = arrayInt.length-1; i >= 0; i--) {

        	System.out.println(arrayInt[i] + "");
        }    
    }   
}
