import java.io.*;
import java.util.*;

public class numSys {
   
    public static String[] digits = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};  
    
    public static String getDigit(int number, int N){   

        return (number == 0) ? "" : getDigit(number / N, N) + digits[number % N];
    } 
 
    public static void main(String[] args) throws IOException{

    	Scanner in = new Scanner(System.in);

    	System.out.println("Введите желаемую систему исчисления( 2, 8, 10, 12, 16): ");

    	int N = in.nextInt();

    	if (N == 2 || N == 8 || N == 10 || N == 12 || N == 16)

    	{

    	System.out.println("Введите число для преобразования: ");

    	int num = in.nextInt();

        System.out.println(getDigit(num, N));

    	}

        else System.out.println("Неверный формат системы исчисления!");
    }
}
