import java.util.*;

public class checkNum {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        System.out.println("Введите число для проверки: ");
        int number = in.nextInt();
        if (isSimple(number, 2) == true) {
        	System.out.println("Простое число!");
        }
        else System.out.println("Не простое число!");
    }
    public static boolean isSimple(int number, int i) {

        if (number == 1) return false; // 1 - не простое число
        if (number == 2) return true; // особый случай
        
        if (number % i == 0) return false;  
        
        if(i <= Math.sqrt(number)) // проверяем не все делители, а только до корня квадратного из числа
            return isSimple(number, i + 1);
        else return true;
    }
}
