import java.util.*;

public class Array {
	public static void main(String[] args) {
        int[] array = new int[]{0, 5, 1, 1, 1, 3, 3, 3, 3, 2, 3, 3, 2, 1, 0, 4};
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(deduplicate(array)));
    }
 
    private static int[] deduplicate(int[] array) {
        int[] result = new int[array.length];
        int n = 0;
        for (int value : array) {
            if (notContains(result, n, value)) {
                result[n++] = value;
            }
        }
        return trim(result, n);
    }
 
    private static boolean notContains(int[] array, int bound, int i) {
        return !contains(array, bound, i);
    }
 
    private static boolean contains(int[] array, int bound, int i) {
        for (int j = 0; j < bound; j++) {
            if (array[j] == i) {
                return true;
            }
        }
        return false;
    }
 
    private static int[] trim(int[] array, int size) {
        int[] result = new int[size];
        System.arraycopy(array,0, result, 0, size);
        return result;
    }
}
