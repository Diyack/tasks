import java.util.*;

public class ArrayS {
	public static void main(String[] args) {
        int[] array = {12, 0, -22, 0, 43, 545, -4, -55, 12, 43, 0, -999, -87};
        divide(array);
    }
 
    private static void divide(int[] array) {
        int positive = 0;
        int negative = 0;
        for (int elem : array) {
            if (elem > 0) positive++;
            else if (elem < 0) negative++;
        }
 
        int[] allPositive = new int[positive];
        int[] allNegative = new int[negative];
        positive = 0;
        negative = 0;
 
        for (int elem : array) {
            if (elem > 0) allPositive[positive++] = elem;
            else if (elem < 0) allNegative[negative++] = elem;
        }
    // результирующий массив будет получем путем сложения отрицательного с положительным
        int length = allNegative.length + allPositive.length;
        int[] result = new int[length];
        System.arraycopy(allNegative, 0, result, 0, allNegative.length);
        System.arraycopy(allPositive, 0, result, allNegative.length, allPositive.length);
        System.out.println(Arrays.toString(result));
    }
}
