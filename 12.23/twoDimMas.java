import java.util.*;

public class twoDimMas {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        System.out.println("Введите 'n': ");
        int n = in.nextInt();
        double k = Math.floor(n / 2);
        // первый массив
        int [][] MasA = new int[n][n];
        	for(int i = 0; i < MasA.length; i++){
        		for(int j = 0; j < MasA[0].length; j++){
                    int l = MasA.length;
                    l--;
        			if (i==j) MasA[i][j]= 1;
                    else if(i==i && j==l-i) MasA[i][j]= 1;
        			else MasA[i][j]= 0;
                    
               	}
        	}
        	System.out.println("Массив А: ");
        for(int i = 0; i < n; i++){
        	for(int j = 0; j < n; j++){
        		System.out.print("" + MasA[i][j] + "");
        	}
        	System.out.println();
    	}
        // второй массив
    	int [][] MasB = new int[n][n];
        	for(int i = 0; i < MasB.length; i++){
        		for(int j = 0; j < MasB[0].length; j++){
                    int l = MasB.length;
                    l--;
        			if (i==j) MasB[i][j]= 1;
                    else if(i==i && j==l-i) MasB[i][j]= 1;
        			else if(i==k) MasB[i][j]= 1;
        			else if(j==k) MasB[i][j]= 1;
        			else MasB[i][j]= 0;
               	}
        	}
        	System.out.println("Массив Б: ");
        for(int i = 0; i < n; i++){
        	for(int j = 0; j < n; j++){
        		System.out.print("" + MasB[i][j] + "");
        	}
        	System.out.println();
    	}
        // третий массив
    	int [][] MasC = new int[n][n];
        	for(int l = (int)k; l <= (int)k+(int)k-1; l++){
                for(int i = 0; i < MasC.length; i++){
        		  for(int j = 0; j < MasC[0].length; j++){
                        if(i == k &&j == k) MasC[i][j]= 1;
                        else if(i == k) MasC[i][j]= 0;
                        else if(i >= 1 && j <= l-i) MasC[i][j]= 0;
                        else if(i >= k && j <= i-l) MasC[i][j]= 0;
                        else MasC[i][j]= 1;
                        MasC[n-1][j] = MasC[0][j];
                        MasC[n-1-i][j] = MasC[i][j];
               	    }
                    for(int j = n-1; j > 0; j--){
                        MasC[i][n-1] = MasC[i][0];
                        MasC[i][j] = MasC[i][n-1-j];
                        MasC[n-1-i][j] = MasC[n-1-i][n-1-j];
                    }
        	   }
            }
        	System.out.println("Массив В: ");
        for(int i = 0; i < n; i++){
        	for(int j = 0; j < n; j++){
        		System.out.print("" + MasC[i][j] + "");
        	}
        	System.out.println();
    	}
    }
}
