import java.util.*;

public class delFromMas {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
    //	int s = 5;
    //	int k = 2;
        Random random = new Random();
        int[][] Mas = new int[11][11];
        for (int i = 0; i < Mas.length; i++) {
            for (int j = 0; j < Mas[i].length; j++) {
                Mas[i][j] = random.nextInt(20);
            }
        }
        System.out.println("Рандомный массив: ");
        show(Mas);
        System.out.println("Примечание! Нумерация строк и столбцов идет от 0! ");
        System.out.println("Введите какой столбец удалить: ");
        int s = in.nextInt();
        if (s >= 0 && s < Mas[0].length){
        	System.out.println("После удаления столбца: ");
        	show(delCol(Mas, s));
    	}
    	else System.out.println("ОШИБКА!");
        System.out.println("Введите какую строку удалить: ");
        int k = in.nextInt();
        if (k >= 0 && k < Mas.length){
        	System.out.println("После удаления строки: ");
        	show(delRow(Mas, k));
    	}
    	else System.out.println("ОШИБКА!");
    }
 
    private static void show(int[][] matrix) {
        for (int[] aMatrix : matrix) {
            for (int anAMatrix : aMatrix) {
                System.out.print(anAMatrix + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
 
    private static int[][] delRow(int[][] matrix, int row) {
        int res[][] = new int[matrix.length - 1][matrix[0].length];
        System.arraycopy(matrix, 0, res, 0, row);
        System.arraycopy(matrix, row + 1, res, row, matrix.length - 1 - row);
        return res;
    }
 
    private static int[][] delCol(int[][] matrix, int col) {
        int res[][] = new int[matrix.length][matrix[0].length - 1];
        for (int i = 0; i < matrix[0].length - 1; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (i >= col) res[j][i] = matrix[j][i + 1];
                else res[j][i] = matrix[j][i];
            }
        }
        return res;
    }
}