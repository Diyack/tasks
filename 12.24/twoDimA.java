import java.util.*;

public class twoDimA {
		
	public static void main(String args[]) {
		Scanner in = new Scanner(System.in);
		System.out.println("Введите размерность массива: ");
        int size = in.nextInt();
        int[][] matrix = new int[size][size];
        // Заполнение первого столбца и строки
        for( int i = 0; i < size; i++ )
        {
             matrix[i][0] = matrix[0][i] = 1;
        }

        // Заполнение оставшейся части матрицы
        for(int row = 1; row < size; row++)
        {
            for(int col = row; col < size; col++)
            {
                matrix[row][col] = matrix[row][col -1] + matrix[row -1][col];
                if( row != col )
                {
                    matrix[col][row] = matrix[row][col];
                }
            }
        }

        // Вывод результата
        for(int row = 0; row < size; row++)
        {
            for(int col = 0; col < size; col++)
            {
                System.out.print(" " + matrix[row][col]);
            }
            System.out.println();
        }
    } 
}
