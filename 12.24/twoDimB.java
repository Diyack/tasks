import java.util.*;

public class twoDimB {
    public static void main(String[] args) {
      	Scanner in = new Scanner(System.in);
		System.out.println("Введите размерность массива: ");
        int size = in.nextInt();
        int[][] matrix = new int[size][size];
        for(int i = 0; i < size; i++)
        {
        	matrix[0][0] = 1;
        	matrix[0][i] = matrix[i][0] = ((i += 0)+1);
        }

        for(int row = 1; row < size; row++)
        {
            for(int col = row; col < size; col++)
            {
                matrix[row][col] = row + col + 1;
                if( matrix[row][col] > size)
                {
                    matrix[row][col] = matrix[row][col] - size;
                }
            }
        }
        for(int col = 1; col < size; col++)
        {
            for(int row = col; row < size; row++)
            {
                matrix[row][col] = row + col + 1;
                if( matrix[row][col] > size)
                {
                    matrix[row][col] = matrix[row][col] - size;
                }
            }
        }
        for(int row = 0; row < size; row++)
        {
            for(int col = 0; col < size; col++)
            {
                System.out.print(" " + matrix[row][col]);
            }
            System.out.println();
        }
    }
}
