import java.util.*;

public class BMas {
    public static void main(String[] args) {
    	// Массив А
    	int [][] firstMas = new int[12][10];
    	int h1 = 1;
    	for (int i = 0; i < 12; i++){
    		for (int j = 0; j < 10; j++){
    			firstMas[i][j] = h1;
    			h1++;
    		}
    	}
    	System.out.println("Массив А: ");
        for(int i = 0; i < 12; i++){
        	for(int j = 0; j < 10; j++){
        		System.out.print("" + firstMas[i][j] + " ");
        	}
        	System.out.println();
        }
        // Массив Б
    	int [][] secondMas = new int[12][10];
    	int h2 = 1;
    	for (int j = 0; j < 10; j++){
    		for (int i = 0; i < 12; i++){
    			secondMas[i][j] = h2;
    			h2++;
    		}
    	}
    	System.out.println("Массив Б: ");
        for(int i = 0; i < 12; i++){
        	for(int j = 0; j < 10; j++){
        		System.out.print("" + secondMas[i][j] + " ");
        	}
        	System.out.println();
        }
        // Массив В
    	int [][] thirdMas = new int[12][10];
    	int h3 = 120;
    	for (int i = 11; i >= 0; i--){
    		for (int j = 0; j < 10; j++){
    			thirdMas[i][j] = h3;
    			h3--;
    		}
    	}
    	System.out.println("Массив В: ");
        for(int i = 0; i < 12; i++){
        	for(int j = 0; j < 10; j++){
        		System.out.print("" + thirdMas[i][j] + " ");
        	}
        	System.out.println();
        }
        // Массив Г
    	int [][] fourthMas = new int[12][10];
    	int h4 = 120;
    	for (int j = 9; j >= 0; j--){
    		for (int i = 0; i < 12; i++){
    			fourthMas[i][j] = h4;
    			h4--;
    		}
    	}
    	System.out.println("Массив Г: ");
        for(int i = 0; i < 12; i++){
        	for(int j = 0; j < 10; j++){
        		System.out.print("" + fourthMas[i][j] + " ");
        	}
        	System.out.println();
        }
        // Массив Д - змейка
    	int [][] fifthMas = new int[10][12];
    	int h5 = 1;
 		boolean dir = true;
        
        for (int i = 0; i < fifthMas.length; i++) {
            if  (dir){
                for (int j = 0; j < fifthMas[i].length; j++) {
                    fifthMas[i][j] = h5++;
                }
                dir = false;
            }
            else {
                for (int j = fifthMas[i].length - 1; j >= 0; j--) {
                    fifthMas[i][j] = h5++;
                }
                dir = true;
            }
        }
    	System.out.println("Массив Д: ");
        for(int i = 0; i < 10; i++){
        	for(int j = 0; j < 12; j++){
        		System.out.print("" + fifthMas[i][j] + " ");
        	}
        	System.out.println();
        }
        // Массив Е - змейка
    	int [][] sixthMas = new int[12][10];
    	int h6 = 1;
    	boolean dir2 = true;
        
        for (int j = 0; j < 10; j++) {
            if  (dir2){
                for (int i = 0; i < 12; i++) {
                    sixthMas[i][j] = h6++;
                }
                dir2 = false;
            }
            else {
                for (int i = 11; i >= 0; i--) {
                    sixthMas[i][j] = h6++;
                }
                dir2 = true;
            }
        }
    	System.out.println("Массив Е: ");
        for(int i = 0; i < 12; i++){
        	for(int j = 0; j < 10; j++){
        		System.out.print("" + sixthMas[i][j] + " ");
        	}
        	System.out.println();
        }
        // Массив Ж
    	int [][] seventhMas = new int[12][10];
    	int h7 = 120;
    	for (int i = 0; i < 12; i++){
    		for (int j = 9; j >= 0; j--){
    			seventhMas[i][j] = h7;
    			h7--;
    		}
    	}
    	System.out.println("Массив Ж: ");
        for(int i = 0; i < 12; i++){
        	for(int j = 0; j < 10; j++){
        		System.out.print("" + seventhMas[i][j] + " ");
        	}
        	System.out.println();
        }
        // Массив З
    	int [][] eighthMas = new int[12][10];
    	int h8 = 1;
    	for (int j = 9; j >= 0; j--){
    		for (int i = 0; i < 12; i++){
    			eighthMas[i][j] = h8;
    			h8++;
    		}
    	}
    	System.out.println("Массив З: ");
        for(int i = 0; i < 12; i++){
        	for(int j = 0; j < 10; j++){
        		System.out.print("" + eighthMas[i][j] + " ");
        	}
        	System.out.println();
        }
        // Массив И
    	int [][] ninthMas = new int[12][10];
    	int h9 = 120;
    	for (int i = 0; i < 12; i++){
    		for (int j = 0; j < 10; j++){
    			ninthMas[i][j] = h9;
    			h9--;
    		}
    	}
    	System.out.println("Массив И: ");
        for(int i = 0; i < 12; i++){
        	for(int j = 0; j < 10; j++){
        		System.out.print("" + ninthMas[i][j] + " ");
        	}
        	System.out.println();
        }
        // Массив К
    	int [][] tenthMas = new int[12][10];
    	int h10 = 120;
    	for (int j = 0; j < 10; j++){
    		for (int i = 0; i < 12; i++){
    			tenthMas[i][j] = h10;
    			h10--;
    		}
    	}
    	System.out.println("Массив К: ");
        for(int i = 0; i < 12; i++){
        	for(int j = 0; j < 10; j++){
        		System.out.print("" + tenthMas[i][j] + " ");
        	}
        	System.out.println();
        }
    }
}
