import java.util.*;

public class SpiralMas {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите размерность масcива n (NxN), где n нечетное число : ");
        int lng = in.nextInt(); 
            if (lng%2 == 1){
            int[][] tbl = new int[lng][lng];
            int x = 0;
            int y = lng - 1;
            int ctr = 1;
            int xo = 0;
            int yo = 0;
        
            while (ctr <= lng * lng) {
                for (int i = 0; i < tbl.length - xo; i++) {
                    if (tbl[x][i] == 0) {
                        tbl[x][i] = ctr++;                  
                    }
                }
            
                for (int i = 0; i < tbl.length - yo; i++) {
                    if (tbl[i][y] == 0) {
                        tbl[i][y] = ctr++;                  
                    }
                }
            
                for (int i = tbl.length - 1 - yo; i >= 0; i--) {
                    if (tbl[y][i] == 0) {
                    tbl[y][i] = ctr++;                  
                    }
                }
            
                for (int i = tbl.length - 1 - xo; i >= 0; i--) {
                    if (tbl[i][x] == 0) {
                        tbl[i][x] = ctr++;                  
                    }
                }
            
                x++;
                y--;
                xo++;
                yo++;
            }
        
            for (int i = 0; i < tbl.length; i++) {
                for (int j = 0; j < tbl.length; j++) {
                    System.out.printf("%2d ", tbl[i][j]);
                }
                System.out.println("");
            }
        }
        else System.out.println("ОШИБКА");
    }
}

