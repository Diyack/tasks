import java.util.*;

public class Base {
    public static void main(String[] args) {
        Database db1 = new Database();
        db1.addEmployee(new Employee("Игнатий", "Лыткин", "Ефимович", "Дербент", 2015, 7));
        db1.addEmployee(new Employee("Петр", "Носков", "Петрович", "Ишим", 2019, 9));
        db1.addEmployee(new Employee("Людвиг", "Громов", "Михайлович", "Комсомольск на Амуре", 2009, 10));
        db1.addEmployee(new Employee("Натан", "Одинцов", "Гордеевич", "Новосибирск", 2018, 12));
        db1.addEmployee(new Employee("Владлен", "Волков", "Борисович", "Талнах", 2008, 11));
        db1.addEmployee(new Employee("Сергей", "Ширяев", "Дмитрьевич", "Курган", 2001, 5));
        db1.addEmployee(new Employee("Авраам", "Коновалов", "Вячеславович", "Горно-Алтайск", 2019, 4));
        db1.addEmployee(new Employee("Юлий", "Соболев", "Игнатьевич", "Рязань", 1999, 3));
        db1.addEmployee(new Employee("Валерий", "Калашников", "Якунович", "Сибай", 2001, 11));
        db1.addEmployee(new Employee("Роман", "Петухов", "Натанович", "Краснодар", 2000, 10));
        db1.addEmployee(new Employee("Элеонора", "Лукина", "Альвионовна", "Кропоткин", 2005, 8));
        db1.addEmployee(new Employee("Юлия", "Овчинникова", "Сергеевна", "Снежинск", 2003, 3));
        db1.addEmployee(new Employee("Алина", "Авдеева", "Мироновна", "Нижнекамск", 2002, 7));
        db1.addEmployee(new Employee("Василиса", "Блохина", "Рудольфовна", "Верхняя Пышма", 2012, 10));
        db1.addEmployee(new Employee("Капитолина", "Сергеева", "Ильяовна", "Выборг", 2017, 2));
        db1.addEmployee(new Employee("Марианна", "Костина", "Христофоровна", "Балахна", 2008, 4));
        db1.addEmployee(new Employee("Станислава", "Сорокина", "Тимуровна", "Муром", 2001, 5));
        db1.addEmployee(new Employee("Элиза", "Николаева", "Геласьевна", "Киселевск", 2007, 12));
        db1.addEmployee(new Employee("Фаина", "Комиссарова", "Ростиславовна", "Киров", 2006, 1));
        db1.addEmployee(new Employee("Златослава", "Григорьева", "Степановна", "Уфа", 2003, 10));
 
        ArrayList<Employee> myLocalDatabase = db1.getDb();
 
        for (Employee aMyDatabase : myLocalDatabase) {
            System.out.println(aMyDatabase.toString());
        }
    }
}

class Database {
    private ArrayList<Employee> db;
 
    Database() {
        db = new ArrayList<>();
    }
 
    public void addEmployee(Employee empl){
        db.add(empl);
    }
 
    public ArrayList<Employee> getDb() {
        return db;
    }
}

class Employee {
    private String name;
    private String patronymic;
    private String surname;
    private String address;
    private int year;
    private int month;
 
    Employee(String name, String patronymic, String surname, String address, int year, int month) {
        this.name = name;
        this.patronymic = patronymic;
        this.surname = surname;
        this.address = address;
        this.year = year;
        this.month = month;
    }
 
    public String toString(){ 
        if ((2020 - this.year > 3) || (2020 - this.year == 3 && this.month <= 9))
        	return String.format("Имя: %s, Фамилия: %s, Отчество: %s\nАдрес: %s\n---------------------------",
                name, patronymic, surname, address);
        else return String.format("");
    }
}