import java.util.Scanner;

public class NumRev {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
    	System.out.println("Введите число больше 100 и меньше 200: ");
    	int input = in.nextInt();
    	int max = 200;
    	int min = 100;
    	if (max > input && input > min) {String str = Integer.toString(input);
       		StringBuffer buffer = new StringBuffer(str);
        	buffer.reverse();
        	System.out.println("Отраженное число: " +buffer);
        }
		else System.out.println("ОШИБКА");
    }
}
