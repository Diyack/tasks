import java.util.*;

public class Num {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
    	System.out.println("Введите число в диапозоне от 100 до 999: ");
    	int input = in.nextInt();
    	int max = 999;
    	int min = 100;
    	if (max >= input && input >= min) {
    		// a,b,c - part of the number
    		int a = Math.floorDiv(input, 100);
    		int b = Math.floorDiv(input, 10) % 10;
    		int c = input % 10;
    		// result = acb = 100 * a + 10 * c + b
    		int result = 100 * a + 10 * c + b;
    		System.out.println("Исходное число: " +result);
    	}
        else System.out.println("ОШИБКА");
    }
}
