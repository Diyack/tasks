import java.util.*;

public class Sin {
    public static void main(String[] args) {
    	// s = 1'' ; m = 1' ; h = 5`
    	Scanner in1 = new Scanner(System.in);
    	System.out.println("Введите часы: ");
    	int h = in1.nextInt();
    	int hr = h % 12;
    	Scanner in2 = new Scanner(System.in);
    	System.out.println("Введите минуты: ");
    	int m = in2.nextInt();
    	Scanner in3 = new Scanner(System.in);
    	System.out.println("Введите секунды: ");
    	int s = in3.nextInt();
		if (h > 0 && h <= 23 && m >= 0 && 59 >= m && s >= 0 && 59 >= s) {
    		// ` = 360''     hr = 5`   >> ` = (5*hr*360 + 60*m + s)/360 = 5*hr + m/60 + s/360
    		double deg = 5 * hr + m/60 + s/360;
    		double rad = Math.toRadians(deg);
    		double result = Math.sin(rad);
    		System.out.println("Синус угла = " +result);
    	}
        else System.out.println("ОШИБКА");
    }
}
