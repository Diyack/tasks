public class bool {
    public static void main(String[] args) {
        //Вычислить значение логического выражения
        //а) не (X или Y) и (не X или не Z);
        //б) не (не X и Y) или (X или не Z);
        //в) X или не Y и не (X или не Z);
        boolean x = false;
        boolean y = false;
        boolean z = false;
        boolean x1 = !x;
        boolean y1 = !y;
        boolean z1 = !z;
        //for a
        boolean aresult1 = !(x | y) & (!x | !z);
        boolean aresult2 = !(x | y) & (!x | !z1);
        boolean aresult3 = !(x | y1) & (!x | !z);
        boolean aresult4 = !(x | y1) & (!x | !z1);
        boolean aresult5 = !(x1 | y) & (!x | !z);
        boolean aresult6 = !(x1 | y) & (!x | !z1);
        boolean aresult7 = !(x1 | y1) & (!x | !z);
        boolean aresult8 = !(x1 | y1) & (!x | !z1);
        System.out.println("Результаты для !(X|Y)&(!X|!Z):");
        System.out.println(aresult1);
        System.out.println(aresult2);
        System.out.println(aresult3);
        System.out.println(aresult4);
        System.out.println(aresult5);
        System.out.println(aresult6);
        System.out.println(aresult7);
        System.out.println(aresult8);
        //for b
        boolean bresult1 = !(!x & y) | (x & !z);
        boolean bresult2 = !(!x & y) | (x & !z1);
        boolean bresult3 = !(!x & y1) | (x & !z);
        boolean bresult4 = !(!x & y1) | (x & !z1);
        boolean bresult5 = !(!x1 & y) | (x1 & !z);
        boolean bresult6 = !(!x1 & y) | (x1 & !z1);
        boolean bresult7 = !(!x1 & y1) | (x1 & !z);
        boolean bresult8 = !(!x1 & y1) | (x1 & !z1);
        System.out.println("Результаты для !(!X&Y)|(X&!Z):");
        System.out.println(bresult1);
        System.out.println(bresult2);
        System.out.println(bresult3);
        System.out.println(bresult4);
        System.out.println(bresult5);
        System.out.println(bresult6);
        System.out.println(bresult7);
        System.out.println(bresult8);
        //for v
        boolean vresult1 = x | !y & (x | !z);
        boolean vresult2 = x | !y & (x | !z1);
        boolean vresult3 = x | !y1 & (x | !z);
        boolean vresult4 = x | !y1 & (x | !z);
        boolean vresult5 = x1 | !y & (x1 | !z);
        boolean vresult6 = x1 | !y & (x1 | !z1);
        boolean vresult7 = x1 | !y1 & (x1 | !z);
        boolean vresult8 = x1 | !y1 & (x1 | !z1);
        System.out.println("Результаты для X|!Y&(X|!Z):");
        System.out.println(vresult1);
      	System.out.println(vresult2);
        System.out.println(vresult3);
        System.out.println(vresult4);
        System.out.println(vresult5);
        System.out.println(vresult6);
        System.out.println(vresult7);
        System.out.println(vresult8);
    }
}
