/**
* Используется для определения правдивости выражения булевой функции
* для заданных переменных
* @author Alexey Kobyakin
* @version 1.0
*/

public class Logic {
    public static void main(String[] args) {
    	/** Аргументы функций */
    	int X = 0;
    	int Y = 0;
    	int Z = 0;

    	if(getA(X, Y) == true){
    		System.out.println("Каждое из чисел X и Y - нечетное");
    	}
    	else System.out.println("Условие не выполнено");
    	
    	if(getB(X, Y) == true){
    		System.out.println("Только одно из чисел X и Y меньше 20");
    	}
    	else System.out.println("Условие не выполнено");
    	
    	if(getC(X, Y) == true){
    		System.out.println("Одно или оба числа X и Y равно нулю");
    	}
    	else System.out.println("Условие не выполнено");
    	
    	if(getD(X, Y, Z) == true){
    		System.out.println("Каждое из чисел X, Y, Z отрицательное");
    	}
    	else System.out.println("Условие не выполнено");
    	
    	if(getE(X, Y, Z) == true){
    		System.out.println("Только одно из чисел X, Y и Z кратно пяти");
    	}
    	else System.out.println("Условие не выполнено");
    	
    	if(getF(X, Y, Z) == true){
    		System.out.println("Хотя бы одно из чисел X, Y, Z больше 100");
    	}
    	else System.out.println("Условие не выполнено");
    }
        // А
        /** 
        * @return true, если
        * каждое из чисел X и Y нечетное
        */
    static boolean getA(int X, int Y){
        boolean A = (X % 2 == 1) && (Y % 2 == 1);
        return A;
    }
        // Б
        /** 
        * @return true, если
        * только одно из чисел X и Y меньше 20
        */
    static boolean getB(int X, int Y){
        boolean B = ((X < 20) && (Y >= 20)) || ((X >= 20) && (Y < 20));
        return B;
    }
        // В
        /** 
        * @return true, если
        * хотя бы одно из чисел X и Y равно нулю
        */
    static boolean getC(int X, int Y){
        boolean C = (X == 0) || (Y == 0);
        return C;
    }
        // Г
        /** 
        * @return true, если
        * каждое из чисел X, Y, Z отрицательное
        */
    static boolean getD(int X, int Y, int Z){
        boolean D = (X < 0) && (Y < 0) && (Z < 0);
        return D;
    }
        // Д
        /** 
        * @return true, если
        * только одно из чисел X, Y и Z кратно пяти
        */
    static boolean getE(int X, int Y, int Z){
        boolean E = ((X % 5 == 0) && (Y % 5 != 0) && (Z % 5 != 0)) || ((X % 5 != 0) && (Y % 5 == 0) && (Z % 5 != 0)) || ((X % 5 != 0) && (Y % 5 != 0) && (Z % 5 == 0));
        return E;
    }
        // Е
        /** 
        * @return true, если
        * хотя бы одно из чисел X, Y, Z больше 100
        */
    static boolean getF(int X, int Y, int Z){
        boolean F = (X > 100) || (Y > 100) || (Z > 100);
        return F;
    }
    
}
