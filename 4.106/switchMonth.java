import java.util.*;

public class switchMonth {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        System.out.println("Введите порядковый номер месяца: ");
        int input = in.nextInt();
        System.out.println(getMonth(input));
    }
    public static List<String> getMonth(int input) {
    	List<String> result = new ArrayList<>();
    	switch (input) {
    		case 1: result.add("Январь");
    		break;
    		case 2: result.add("Февраль");
    		break;
    		case 3: result.add("Март");
    		break;
    		case 4: result.add("Апрель");
    		break;
    		case 5: result.add("Май");
    		break;
    		case 6: result.add("Июнь");
    		break;
    		case 7: result.add("Июль");
    		break;
    		case 8: result.add("Август");
    		break;
    		case 9: result.add("Сентябрь");
    		break;
    		case 10: result.add("Октябрь");
    		break;
    		case 11: result.add("Ноябрь");
    		break;
    		case 12: result.add("Декабрь");
    		break;
    		default: System.out.println("ОШИБКА");
    		break;
    	}
        return result;
    }
}
