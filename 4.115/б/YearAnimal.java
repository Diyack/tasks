import java.util.*;

public class YearAnimal {
    public static void main(String[] args) {
      Scanner in = new Scanner(System.in);
        System.out.println("Введите год числом ");
        int input = in.nextInt();
        if (input > 1984) System.out.println(getYear(input));
        else System.out.println(getYear1(input));
    }
    public static List<String> getYear(int input) {
      List<String> result = new ArrayList<>();
      switch((input - 1984) % 12) {
        case 0:
          switch (input % 10) {
            case 0: System.out.println("Белая крыса");
               break;
            case 6: System.out.println("Красная крыса");
               break;
            case 8: System.out.println("Желтая крыса");
               break;
            case 4: System.out.println("Зеленая крыса");
               break;
            case 2: System.out.println("Синяя крыса");
               break;
          } //мышь
          break;
        case 1:
          switch (input % 10) {
            case 1: System.out.println("Белый бык");
              break;
            case 7: System.out.println("Красный бык");
              break;
            case 9: System.out.println("Желтый бык");
              break;
            case 5: System.out.println("Зеленый бык");
              break;
            case 3: System.out.println("Синий бык");
              break;
          } // бык
          break;
        case 2:
          switch (input % 10) {
            case 0: System.out.println("Белый тигр");
              break;
            case 6: System.out.println("Красный тигр");
              break;
            case 8: System.out.println("Желтый тигр");
              break;
            case 4: System.out.println("Зеленый тигр");
              break;
            case 2: System.out.println("Синий тигр");
              break;
          } // тиг
          break;
        case 3:
          switch (input % 10) {
            case 1: System.out.println("Белый заяц");
              break;
            case 7: System.out.println("Красный заяц");
              break;
            case 9: System.out.println("Желтый заяц");
              break;
            case 5: System.out.println("Зеленый заяц");
              break;
            case 3: System.out.println("Синий заяц");
              break;
          } // кролик или кот
          break;
        case 4:
          switch (input % 10) {
            case 0: System.out.println("Белый дракон");
              break;
            case 6: System.out.println("Красный дракон");
              break;
            case 8: System.out.println("Желтый дракон");
              break;
            case 4: System.out.println("Зеленый дракон");
              break;
            case 2: System.out.println("Синий дракон");
              break;
          } // дракон
          break;
        case 5:
          switch (input % 10) {
            case 1: System.out.println("Белая змея");
              break;
            case 7: System.out.println("Красная змея");
              break;
            case 9: System.out.println("Желтая змея");
              break;
            case 5: System.out.println("Зеленая змея");
              break;
            case 3: System.out.println("Синяя змея");
              break;
          } // змея
          break;
        case 6:
          switch (input % 10) {
            case 0: System.out.println("Белая лошадь");
              break;
            case 6: System.out.println("Красная лошадь");
              break;
            case 8: System.out.println("Желтая лошадь");
              break;
            case 4: System.out.println("Зеленая лошадь");
              break;
            case 2: System.out.println("Синяя лошадь");
              break;
          } // лошадь
          break;
        case 7:
          switch (input % 10) {
            case 1: System.out.println("Белая овца");
              break;
            case 7: System.out.println("Красная овца");
              break;
            case 9: System.out.println("Желтая овца");
              break;
            case 5: System.out.println("Зеленая овца");
              break;
            case 3: System.out.println("Синяя овца");
              break;
          } // Овца или коза
          break;
        case 8:
          switch (input % 10) {
            case 0: System.out.println("Белая обезьяна");
              break;
            case 6: System.out.println("Красная обезьяна");
              break;
            case 8: System.out.println("Желтая обезьяна");
              break;
            case 4: System.out.println("Зеленая обезьяна");
              break;
            case 2: System.out.println("Синяя обезьяна");
              break;
          } // Обезьяна
          break;
        case 9:
          switch (input % 10) {
            case 1: System.out.println("Белый петух");
              break;
            case 7: System.out.println("Красный петух");
              break;
            case 9: System.out.println("Желтый петух");
              break;
            case 5: System.out.println("Зеленый петух");
              break;
            case 3: System.out.println("Синий петух");
              break;
          } // Петух
          break;
        case 10:
          switch (input % 10) {
            case 0: System.out.println("Белая собака");
              break;
            case 6: System.out.println("Красная собака");
              break;
            case 8: System.out.println("Желтая собака");
              break;
            case 4: System.out.println("Зеленая собака");
              break;
            case 2: System.out.println("Синяя собака");
              break;
          } // собака
          break;
        case 11:
          switch (input % 10) {
            case 1: System.out.println("Белая свинья");
              break;
            case 7: System.out.println("Красная свинья");
              break;
            case 9: System.out.println("Желтая свинья");
              break;
            case 5: System.out.println("Зеленая свинья");
              break;
            case 3: System.out.println("Синяя свинья");
              break;
          } // Бык или свинья
          break;
        default:
          break;
      }
      return result;
    }
    public static List<String> getYear1(int input) {
      List<String> result = new ArrayList<>();
      switch((1984 - input) % 12) {
        case 0:
          switch (input % 10) {
            case 0: System.out.println("Белая крыса");
               break;
            case 6: System.out.println("Красная крыса");
               break;
            case 8: System.out.println("Желтая крыса");
               break;
            case 4: System.out.println("Зеленая крыса");
               break;
            case 2: System.out.println("Синяя крыса");
               break;
          } //мышь
          break;
        case 11:
          switch (input % 10) {
            case 1: System.out.println("Белый бык");
              break;
            case 7: System.out.println("Красный бык");
              break;
            case 9: System.out.println("Желтый бык");
              break;
            case 5: System.out.println("Зеленый бык");
              break;
            case 3: System.out.println("Синий бык");
              break;
          } // бык
          break;
        case 10:
          switch (input % 10) {
            case 0: System.out.println("Белый тигр");
              break;
            case 6: System.out.println("Красный тигр");
              break;
            case 8: System.out.println("Желтый тигр");
              break;
            case 4: System.out.println("Зеленый тигр");
              break;
            case 2: System.out.println("Синий тигр");
              break;
          } // тиг
          break;
        case 9:
          switch (input % 10) {
            case 1: System.out.println("Белый заяц");
              break;
            case 7: System.out.println("Красный заяц");
              break;
            case 9: System.out.println("Желтый заяц");
              break;
            case 5: System.out.println("Зеленый заяц");
              break;
            case 3: System.out.println("Синий заяц");
              break;
          } // кролик или кот
          break;
        case 8:
          switch (input % 10) {
            case 0: System.out.println("Белый дракон");
              break;
            case 6: System.out.println("Красный дракон");
              break;
            case 8: System.out.println("Желтый дракон");
              break;
            case 4: System.out.println("Зеленый дракон");
              break;
            case 2: System.out.println("Синий дракон");
              break;
          } // дракон
          break;
        case 7:
          switch (input % 10) {
            case 1: System.out.println("Белая змея");
              break;
            case 7: System.out.println("Красная змея");
              break;
            case 9: System.out.println("Желтая змея");
              break;
            case 5: System.out.println("Зеленая змея");
              break;
            case 3: System.out.println("Синяя змея");
              break;
          } // змея
          break;
        case 6:
          switch (input % 10) {
            case 0: System.out.println("Белая лошадь");
              break;
            case 6: System.out.println("Красная лошадь");
              break;
            case 8: System.out.println("Желтая лошадь");
              break;
            case 4: System.out.println("Зеленая лошадь");
              break;
            case 2: System.out.println("Синяя лошадь");
              break;
          } // лошадь
          break;
        case 5:
          switch (input % 10) {
            case 1: System.out.println("Белая овца");
              break;
            case 7: System.out.println("Красная овца");
              break;
            case 9: System.out.println("Желтая овца");
              break;
            case 5: System.out.println("Зеленая овца");
              break;
            case 3: System.out.println("Синяя овца");
              break;
          } // Овца или коза
          break;
        case 4:
          switch (input % 10) {
            case 0: System.out.println("Белая обезьяна");
              break;
            case 6: System.out.println("Красная обезьяна");
              break;
            case 8: System.out.println("Желтая обезьяна");
              break;
            case 4: System.out.println("Зеленая обезьяна");
              break;
            case 2: System.out.println("Синяя обезьяна");
              break;
          } // Обезьяна
          break;
        case 3:
          switch (input % 10) {
            case 1: System.out.println("Белый петух");
              break;
            case 7: System.out.println("Красный петух");
              break;
            case 9: System.out.println("Желтый петух");
              break;
            case 5: System.out.println("Зеленый петух");
              break;
            case 3: System.out.println("Синий петух");
              break;
          } // Петух
          break;
        case 2:
          switch (input % 10) {
            case 0: System.out.println("Белая собака");
              break;
            case 6: System.out.println("Красная собака");
              break;
            case 8: System.out.println("Желтая собака");
              break;
            case 4: System.out.println("Зеленая собака");
              break;
            case 2: System.out.println("Синяя собака");
              break;
          } // собака
          break;
        case 1:
          switch (input % 10) {
            case 1: System.out.println("Белая свинья");
              break;
            case 7: System.out.println("Красная свинья");
              break;
            case 9: System.out.println("Желтая свинья");
              break;
            case 5: System.out.println("Зеленая свинья");
              break;
            case 3: System.out.println("Синяя свинья");
              break;
          } // Бык или свинья
          break;
        default:
          break;
      }
      return result;
    }
}