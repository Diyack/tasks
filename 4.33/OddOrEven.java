import java.util.*;

public class OddOrEven {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        System.out.println("Введите число");
        int number = in.nextInt();
        System.out.println("Число " + numberIsOddOrEven(number));
    }

    private static String numberIsOddOrEven(int number) {
        switch (number % 2) {
            case 0:
                return "четное";
            default:
                return "нечетное";
        }
    }
}