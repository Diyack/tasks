import java.io.*;
import java.util.Scanner;

public class light {
    public static void main(String[] args) throws Exception
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите время прошедшее с начала часа");
        double time = sc.nextDouble();

        double res = time % 5;

        if (res >= 0 && res <3)
            System.out.println("Светофор зеленый");
        else if (res >= 3 && res <5)
            System.out.println("Светофор красный");
    }
}