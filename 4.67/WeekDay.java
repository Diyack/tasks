import java.util.*;

public class WeekDay {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите день в году в диапозоне 1-365");
        int input = in.nextInt();
        if (0 < input && input <= 365){
        	if (1 <= check(input) && check(input) <= 5){
        		System.out.println("Рабочий день");
        	}
        	else System.out.println("Выходной день");
        }
        else System.out.println("ОШИБКА");
    }
    private static int check(int input) {
    	int result = input%7;
    	return result;
    }
}
