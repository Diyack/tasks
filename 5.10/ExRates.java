import java.util.*;

public class ExRates {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        System.out.println("Введите курс доллара ");
        double input = in.nextDouble();
        Formatter exr;

        for(int i=1; i <= 20; i++) {
        	exr = new Formatter();
        	exr.format("%d %.2f", i, i * input);
        	System.out.println(exr);
        	exr.close();
        }
    }
}
