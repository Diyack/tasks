import java.util.*;

public class PopDensity {
    public static void main(String[] args) {
    	// population density = number of people / land area
    	Scanner scanner1 = new Scanner(System.in);
    	double[] np = new double[12];
    	double sumnp = 0;
    	Scanner scanner2 = new Scanner(System.in);
    	double[] la = new double[12];
    	double sumla = 0;
    	double pd = 0;
        System.out.println("Введите численность в тыс. чел.: ");
        for (int i=0; i < 12; i++) {
        	np[i] = scanner1.nextDouble();
        }
        for(int i=0; i < 12; i++) {
        	sumnp = sumnp + np[i];
        }
        System.out.println("Введите площать района в кв.км: ");
        for (int i=0; i < 12; i++) {
        	la[i] = scanner2.nextDouble();
        }
        for (int i=0; i < 12; i++){
        	sumla = sumla + la[i];
        }
        pd = sumnp / sumla;
        System.out.println("Суммарная площадь населения: " + pd);
    }
}
