import java.util.*;

public class Print {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        System.out.println("Введите число n: ");
        int n = in.nextInt();
        Formatter prnt;

        for(int i=1; i <= Math.sqrt(n); i++){
        	prnt = new Formatter();
        	prnt.format("%d", i * i);
        	System.out.println(prnt);
        	prnt.close();
        }
    }
}
