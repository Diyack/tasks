import java.util.*;

public class Game {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
    	System.out.println("Enter team #1: ");
    	String team1 = in.nextLine();
        System.out.println("Enter team #2: ");
        String team2 = in.nextLine();
        play(0, 0, team1, team2);
    }

    public static void play(int sc1, int sc2, String team1, String team2) {

    	Scanner ingame = new Scanner(System.in);
    	System.out.println("Enter team to score (1 or 2 or 0 to finish game): ");
        int tn = ingame.nextInt();
        if (tn == 1 || tn == 2){
        	if (tn == 1){
        		System.out.println("Enter score (1 or 2 or 3): ");
        		int sc = ingame.nextInt();
        		if(sc == 1 || sc == 2 || sc == 3){
        			sc1 += sc;
        		}
        		else System.out.println("Error!");
        	}
        	else if (tn == 2){
        		System.out.println("Enter score (1 or 2 or 3): ");
        		int sc = ingame.nextInt();
        		if(sc == 1 || sc == 2 || sc == 3){
        			sc2 += sc;
        		}
        		else System.out.println("Error!");
        	}
        	System.out.println(score(sc1, sc2));
        }
        else if (tn == 0){
        	System.out.println(result(sc1, sc2, team1, team2));
        }
        else System.out.println("Error!");
    	if (tn != 0){
    		play(sc1, sc2, team1, team2);
    	}
    	else {

    	}
    }

    public static String score(int sc1, int sc2) {
    	String score = sc1 + ":" + sc2;
        return score;

    }

    public static String result(int sc1, int sc2, String team1, String team2) {
    	String result = "";
    	if(sc1 > sc2){
    		result = "Winner " + team1;
    	}
    	else if(sc1 < sc2){
    		result = "Winner " + team2;
    	}
    	else if(sc1 == sc2){
    		result = "Draw";
    	}
    	return result;
    }
}
