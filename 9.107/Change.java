import java.util.Scanner;

public class Change {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
    	StringBuilder builder = new StringBuilder(str.toLowerCase());
		int a = builder.indexOf("а");
		int o = builder.lastIndexOf("о");
 
		if (a != -1 && o != -1) {
    		builder.setCharAt(a, 'о');
    		builder.setCharAt(o, 'а');
    		System.out.println(builder.toString());
		}
		else {
			System.out.println("Символ 'а' или 'о' не найден");
		}
	}
}