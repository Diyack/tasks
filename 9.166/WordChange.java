import java.util.*;

public class WordChange {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
    	System.out.println("Введите предложение:");
    	String input = in.nextLine();
        String str = input.trim();
        char wordSplitter = ' ';
        System.out.println(getTransformString(str, str.indexOf(wordSplitter), str.lastIndexOf(wordSplitter)));
    }
 
    private static String getTransformString(String transfStr, int k, int s) {
        String firstPart = transfStr.substring(0, k);
        String secondPart = transfStr.substring(k, s);
        return transfStr.substring(s).concat(secondPart + " ")
                .concat(firstPart);
    }
}
