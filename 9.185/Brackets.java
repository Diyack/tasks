import java.util.*;

public class Brackets {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
    	System.out.println("Введите выражение: ");
        String s = in.nextLine();
        int check = 0;
        for (int i = 0; i < s.length(); i++) {
            String symbol = s.substring(i, i + 1);
            if (symbol.equals("(") || symbol.equals("{") || symbol.equals("[")) {
                check++;
                continue;
            }
            if (symbol.equals(")") || symbol.equals("}") || symbol.equals("]"))
                check--;
        }
        if (check == 0) {
            System.out.println("ДА. Скобки раставлены верно(или отсутсвуют)");
        } 
        else if(check > 0) {
            System.out.println("НЕТ. Открывающих сбокоб больше на " + check);
        } 
        else if (check < 0) {
        	System.out.println("НЕТ. Закравающих скобок больше на " + Math.abs(check));
        }
    }
}