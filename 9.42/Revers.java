import java.util.*;

public class Revers {
    public static void main(String[] args) {
    	Scanner in = new Scanner(System.in);
        System.out.println("Введите слово: ");
        String s = in.nextLine();
        StringBuilder sBuilder = new StringBuilder(s);
        sBuilder.reverse();
        System.out.println(sBuilder.toString());
    }
}
