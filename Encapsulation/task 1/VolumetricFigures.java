import java.util.*;

public class VolumetricFigures {
	private static double differenceOfVol(double sumFigVol, double sumColFigVol){
		return sumFigVol - sumColFigVol;
	}
	
	public static void main(String[] args) {
			double sumFigVol = 0;
			double sumColFigVol = 0;

			Ball ball1 = new Ball();
			ball1.setColor(true);
			ball1.setVolume(150);

			Ball ball2 = new Ball();
			ball2.setColor(true);
			ball2.setVolume(100);

			Parallelepiped parallelepiped1 = new Parallelepiped();
			parallelepiped1.setVolume(200);

			Cube cube1 = new Cube();
			cube1.setVolume(100);

			List list1 = new List();
			list1.addFigure(new Figure(ball1));
			list1.addFigure(new Figure(ball2));
			list1.addFigure(new Figure(parallelepiped1));
			list1.addFigure(new Figure(cube1));

			ArrayList<Figure> figurelist = list1.getList();

			for(Figure listOfFigures : figurelist){
				sumFigVol += listOfFigures.getAllVol();
				sumColFigVol += listOfFigures.getColVol();
			}

			System.out.println("Разность объема всех фигур и цветных: " + differenceOfVol(sumFigVol, sumColFigVol));
	}
}

class Figures{
	private double volume;

	public double getVolume(){
		return volume;
	}
	public void setVolume(double volume){
		this.volume = volume;
	}
}

class Cube extends Figures{
	private double side;

	Cube(){

	}

	Cube(double side){
		this.volumeCalculation(side);
	}

	public void volumeCalculation(double side){
		double volume = side * side * side;
		this.setVolume(volume);
	}
}

class Parallelepiped extends Figures{
	private double width;
	private double length;
	private double height;

	Parallelepiped(){

	}

	Parallelepiped(double width, double length, double height){
		this.volumeCalculation(width, length, height);
	}

	public void volumeCalculation(double width, double length, double height){
		double volume = width * length * height;
		this.setVolume(volume);
	}
}

class Ball extends Figures{
	private Boolean color;
	private double radius;

	Ball(){

	}

	Ball(double radius, Boolean color){
		this.volumeCalculation(radius);
		this.color = color;
	}

	public void setColor(Boolean color){
		this.color = color;
	}
	public Boolean getColor(){
		return color;
	}
	public void volumeCalculation(double radius){
		double volume = (4 * Math.PI * radius * radius * radius) / 4;
		this.setVolume(volume);
	}
}

class Figure{
	private double volume;
	private Boolean color;

	Figure(double volume, Boolean color){
		this.volume = volume;
		this.color = color;
	}

	Figure(Ball ball){
		this.volume = ball.getVolume();
		this.color = ball.getColor();
	}

	Figure(Cube cube){
		this.volume = cube.getVolume();
		this.color = false;
	}

	Figure(Parallelepiped parallelepiped){
		this.volume = parallelepiped.getVolume();
		this.color = false;
	}

	public double getAllVol(){
		return this.volume;
	}

	public double getColVol(){
		if (this.color.equals(true))
			return this.volume;
		else return 0;
	}

}

class List{
	private ArrayList<Figure> list;

	List(){
		list = new ArrayList<>();
	}

	public void addFigure(Figure figure){
		list.add(figure);
	}
	public ArrayList<Figure> getList(){
		return list;
	}
}