public class Product {
    private Size size;
    private Boolean sold;
    public Boolean getSold(){
    	return sold;
    }
    public void setSold(Boolean sold){
    	this.sold = sold;
    }
    public Size getSize(){
    	return size;
    }
    public void setSize(Size size){
    	this.size = size;
    }

    Product(){
    	setSold(false);
    }
}

class Size {
	private double width;
	private double length;
	private double height;
	public void setWidth(double width){
		this.width = width;
	}
	public double getWidth(){
		return width;
	}
	public void setLength(double length){
		this.length = length;
	}
	public double getLength(){
		return length;
	}
	public void setHeight(double height){
		this.height = height;
	}
	public double getHeight(){
		return height;
	}

	Size(){
		setWidth(1);
    	setLength(1);
    	setHeight(1);
	}

	Size(double width, double length, double height){
		setWidth(width);
    	setLength(length);
    	setHeight(height);
	}
}

