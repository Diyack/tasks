import java.awt.Color;
import java.util.*;

public class AutoPark {
    public static void main(String[] args) {
    	double sumDuration = 0;

    	Driver driver1 = new Driver();
    	driver1.setDrivingExpirience(9);
    	driver1.setQualification(1);

    	Driver driver2 = new Driver();
    	driver2.setDrivingExpirience(25);
    	driver2.setQualification(2);

    	Driver driver3 = new Driver();
    	driver3.setDrivingExpirience(11);
    	driver3.setQualification(2);

    	Driver driver4 = new Driver();
    	driver4.setDrivingExpirience(1);
    	driver4.setQualification(0);

    	Car car1 = new Car();
    	car1.setMark("ford");
    	car1.setAge(8);

    	Car car2 = new Car();
    	car2.setMark("mazda");
    	car2.setAge(12);

    	Car car3 = new Car();
    	car3.setMark("ford");
    	car3.setAge(10);

    	Car car4 = new Car();
    	car4.setMark("bmw");
    	car4.setAge(5);

    	Route route1 = new Route();
    	route1.setDuration(15);
    	route1.setStops(3);

    	Route route2 = new Route();
    	route2.setDuration(70);
    	route2.setStops(20);

    	Route route3 = new Route();
    	route3.setDuration(30);
    	route3.setStops(15);

    	Route route4 = new Route();
    	route4.setDuration(22);
    	route4.setStops(11);

    	Journal journal1 = new Journal();
    	journal1.addRouteList(new RouteList(driver3, car2, route2));
    	journal1.addRouteList(new RouteList(driver4, car1, route1));
    	journal1.addRouteList(new RouteList(driver1, car3, route4));
    	journal1.addRouteList(new RouteList(driver2, car4, route3));

    	ArrayList<RouteList> todayJournal = journal1.getJournal();

    	for(RouteList daylyJournal : todayJournal){
    		sumDuration += daylyJournal.getSumDuration();
    	}

    	System.out.println("Общее расстояние пройденное водителями на машине марки Ford, со стажем меньше 10 лет: " + sumDuration + "км.");
    }
}

class Driver{
	private int drivingExpirience;
	private int qualification;
	public int getDrivingExpirience(){
		return drivingExpirience;
	}
	public void setDrivingExpirience(int expirience){
		this.drivingExpirience = expirience;
	}
	public String getQualification(){
		if (qualification == 0){
			return "Стажер";
		}
		else if(qualification == 1){
			return "Водитель";
		}
		else if(qualification == 2){
			return "Мастер";
		}
		else return "Error in qualification!";
	}
	public void setQualification(int qualification){
		this.qualification = qualification;
	}
}

class Car{
	private String mark;
	private Color color;
	private int age;
	public String getMark(){
		return mark;
	}
	public void setMark(String mark){
		this.mark = mark;
	}
	public Color getColor(){
		return color;
	}
	public void setColor(Color color){
		this.color = color;
	}
	public int getAge(){
		return age;
	}
	public void setAge(int age){
		this.age = age;
	}
}

class Route{
	private double duration;
	private int stops;
	public double getDuration(){
		return duration;
	}
	public void setDuration(double duration){
		this.duration = duration;
	}
	public int getStops(){
		return stops;
	}
	public void setStops(int stops){
		this.stops = stops;
	}
}

class RouteList{
	private int drivingExpirience;
	private String mark;
	private double duration;

	RouteList(Driver driver, Car car, Route route){
		this.drivingExpirience = driver.getDrivingExpirience();
		this.mark = car.getMark();
		this.duration = route.getDuration();
	}

	public double getSumDuration(){
		if (this.drivingExpirience <= 10 && this.mark.equals("ford"))
			return this.duration;
		else return 0;
	}

}

class Journal{
	private ArrayList<RouteList> journal;

	Journal(){
		journal = new ArrayList<>();
	}

	public void addRouteList(RouteList routelist){
		journal.add(routelist);
	}
	public ArrayList<RouteList> getJournal(){
		return journal;
	}
}
