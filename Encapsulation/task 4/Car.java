import java.awt.Color;
import java.util.*;

public class Car {
    private Body body;
    private String brand;
    private String model;
    private int yearOfIssue;
    private Engine engine;
    private Color color;
    private Boolean airbag;
    private Chassis chassis;

    public Body getBody(){
        return body;
    }
    public void setBody(Body ob){
        this.body = ob;
    }
    public Engine getEngine(){
        return engine;
    }
    public void setEngine(Engine ob){
        this.engine = ob;
    }
    public Color getColor(){
    	return color;
    }
    public void setColor(Color color){
    	this.color = color;
    }
    public int age(int year){
    	return year - yearOfIssue;
    }
    public int getYearOfIssue(){
    	return yearOfIssue;
    }
    public void setYearOfIssue(int year){
    	this.yearOfIssue = year;
    }
    public String getBrand(){
        return brand;
    }
    public void setBrand(String brand){
        this.brand = brand;
    }
    public String getModel(){
        return model;
    }
    public void setModel(String model){
        this.model = model;
    }
    public Boolean getAirbag(){
        return airbag;
    }
    public void setAirbag(Boolean airbag){
        this.airbag = airbag;
    }
    public Chassis getChassis(){
        return chassis;
    }
    public void setChassis(Chassis ob){
        this.chassis = ob;
    }

    Car(){

    }

    Car(Body body, String brand, String model, int yearOfIssue, Engine engine, Color color, Boolean airbag, Chassis chassis){
        setBody(body);
        setBrand(brand);
        setModel(model);
        setYearOfIssue(yearOfIssue);
        setEngine(engine);
        setColor(color);
        setAirbag(airbag);
        setChassis(chassis);
    }

    public static void main(String[] args) {
        Color red = new Color(255, 0, 0);
        Color blue = new Color(0, 0, 255);
        Color green = new Color(0, 255, 0);
        Color white = new Color(255, 255, 255);
        Color black = new Color(0, 0, 0);

        Chassis chassis1 = new Chassis();
        chassis1.setDriveType("front-wheel drive");
        chassis1.setWheels(4);
        Chassis chassis2 = new Chassis();
        chassis2.setDriveType("rear drive");
        chassis2.setWheels(4);
        Chassis chassis3 = new Chassis();
        chassis3.setDriveType("four-wheel drive");
        chassis3.setWheels(4);

       //Bentley Flying Spur III
        Engine engine1 = new Engine();
        engine1.setFuelType("benzine");
        engine1.setEngineName("4.0 V8 AWD Automatic ");
        engine1.setPower(550);
        engine1.setTorque(6000);
        engine1.setFuelConsumption(12.7);

        Body body1 = new Body();
        body1.setBodyType("Sedan");
        body1.setVisualOb(3);

        Car car1 = new Car(body1, "Bentley", "Flying Spur III", 2020, engine1, red, true, chassis3);
        Car car2 = new Car(body1, "Bentley", "Flying Spur III", 2020, engine1, blue, true, chassis3);
        Car car3 = new Car(body1, "Bentley", "Flying Spur III", 2020, engine1, black, true, chassis3);
        Car car4 = new Car(body1, "Bentley", "Flying Spur III", 2020, engine1, white, true, chassis3);
        Car car5 = new Car(body1, "Bentley", "Flying Spur III", 2020, engine1, green, true, chassis3);


        CarList carlist1 = new CarList();
        carlist1.addCar(new ListOfCar (car1));
        carlist1.addCar(new ListOfCar (car2));
        carlist1.addCar(new ListOfCar (car3));
        carlist1.addCar(new ListOfCar (car4));
        carlist1.addCar(new ListOfCar (car5));

        ArrayList<ListOfCar> cars = carlist1.getCarList();

        for(ListOfCar car : cars){
            car.carInfo();
        }
    }
}

class Engine {
    private String fuelType;
    private String engineName;
    private int power;
    private int torque;
    private double fuelConsumption;

    public String getFuelType(){
        return fuelType;
    }
    public void setFuelType(String fuelType){
        this.fuelType = fuelType;
    }
    public String getEngineName(){
        return engineName;
    }
    public void setEngineName(String engineName){
        this.engineName = engineName;
    }
    public int getPower(){
        return power;
    }
    public void setPower(int power){
        this.power = power;
    }
    public int getTorque(){
        return torque;
    }
    public void setTorque(int torque){
        this.torque = torque;
    }
    public double getFuelConsumption(){
        return fuelConsumption;
    }
    public void setFuelConsumption(double fuelConsumption){
        this.fuelConsumption = fuelConsumption;
    }

    Engine(){

    }
}

class Body {
    private String bodyType;
    private double visualOb;

    public String getBodyType(){
        return bodyType;
    }
    public void setBodyType(String bodyType){
        this.bodyType = bodyType;
    }
    public double getVisualOb(){
        return visualOb;
    }
    public void setVisualOb(double visualOb){
        this.visualOb = visualOb;
    }

    Body(){

    }
}

class Chassis {
    private int wheels;
    private String driveType;

    public int getWheels(){
        return wheels;
    }
    public void setWheels(int wheels){
        this.wheels = wheels;
    }
    public String getDriveType(){
        return driveType;
    }
    public void setDriveType(String driveType){
        this.driveType = driveType;
    }

    Chassis(){

    }
}

class CarList {
    private ArrayList<ListOfCar> carlist;

    CarList(){
        carlist = new ArrayList<>();
    }

    public void addCar(ListOfCar car){
        carlist.add(car);
    }
    public ArrayList<ListOfCar> getCarList(){
        return carlist;
    }
}

class ListOfCar {
    private String bodyType;
    private String brand;
    private String model;
    private Color color;
    private String driveType;
    private int power;
    private String engineName;
    private String fuelType;
    private double fuelConsumption;

    ListOfCar(Car ob){
        this.bodyType = ob.getBody().getBodyType();
        this.brand = ob.getBrand();
        this.model = ob.getModel();
        this.color = ob.getColor();
        this.driveType = ob.getChassis().getDriveType();
        this.power = ob.getEngine().getPower();
        this.engineName = ob.getEngine().getEngineName();
        this.fuelType = ob.getEngine().getFuelType();
        this.fuelConsumption = ob.getEngine().getFuelConsumption();
    }

    public void carInfo(){ 
         System.out.println("Название: " + this.brand + " " + this.model + " " + this.color + "Кузов: " + this.bodyType + " " + "Привод: " + this.driveType + "Двигатель: " + this.engineName + "Мощность: " + this.power + "лс");
    }
}