import java.util.*;
import java.util.function.Consumer;

interface Say{
	void hello();
}

interface Experience{
	void describeExperience();
}

public class Interview {
    public static void main(String[] args) {
    	Employer employer = new Employer();

    	List<Candidate> list1 = new ArrayList<>();
    	list1.add(new Candidate("Игорь", true));
    	list1.add(new Candidate("Андрей", false));
    	list1.add(new Candidate("Олег", true));
    	list1.add(new Candidate("Александр", false));
    	list1.add(new Candidate("Илья", true));
    	list1.add(new Candidate("Матвей", false));
    	list1.add(new Candidate("Иван", true));
    	list1.add(new Candidate("Руслан", false));
    	list1.add(new Candidate("Виктор", true));
    	list1.add(new Candidate("Кирилл", false));

    	Consumer<Candidate> talk = x->{
    		employer.hello();
    		x.hello();
    		x.describeExperience();
    		System.out.println("");
    	};

    	list1.stream().forEach(x -> talk.accept(x));
    }
}

abstract class Human implements Say{

	public void hello(){
		System.out.println("Привет");
	}

}

class Employer extends Human{
	
	Employer(){

	}

	public void hello(){
		System.out.println("Здравствуйте, представьтесь пожалуйста и опишите ваш опыт");
	}
}

class Candidate extends Human implements Experience{
	private String name;
	private Boolean experience;

	Candidate(String name, Boolean experience){
		this.name = name;
		this.experience = experience;
	}
	
	public void hello(){
		System.out.println("Здравствуйте, меня зовут " + this.name);
	}

	public void describeExperience(){
		if (this.experience.equals(true)){
			System.out.println("Я обучался Java самостоятельно, и никто не проверял насколько хорош мой код");
		}
		else System.out.println("Я успешно прошел курс KJ school и публичное код ревью");
	}
}


